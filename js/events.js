window.onload = function(){
	init();
}

function init(){

 	// Declare function using event object
	function minChars(event){
		var target = event.target;
		var error = target.nextElementSibling;
	  
		if(target.value.length < 8){
	     error.style.display = 'block';
	     error.innerHTML = target.name + ' must be 8 characters or more';
		}else{
	     error.style.display = 'none';
	     error.innerHTML = '';
	  }
	}

	function validEmail(event){
		var target = event.target;
		var error = target.nextElementSibling;

		if(!target.value.includes('@')){
			error.style.display = 'block';
			error.innerHTML = target.name + ' must be a valid address';
		}else{
			error.style.display = 'none';
			error.innerHTML = '';
		}
	}

	// CTA locations
	var cta = document.getElementsByClassName('cta');
	// var selected = document.getElementsByClassName('selected');

	// Loop through list and add id to first article
	for(var i = 0; i < cta.length; i++){
		cta[0].id = 'current';	
	}

	// element with id current
	var current = document.getElementById('current');

	function changeRight(){

		var num = 0;

		// loop through cta list
		for(var i = 0; i < cta.length; i++){
			// if element matches node with current id
			if(cta[i] === current){
				// remove id and add to next element
				cta[i].id = '';

				if(cta[num + 1] == undefined){
					var nextArticle = cta[0];
					nextArticle.id = 'current';
				
				}else{
					var nextArticle = cta[num + 1];
					nextArticle.id = 'current';
				}
			}
			num++;
		}
		current = nextArticle;
			
	}

	function changeLeft(){

		var num = 0;

		// loop through cta list
		for(var i = 0; i < cta.length; i++){
			// if element matches node with current id
			if(cta[i] === current){
				// remove id and add to next element
				cta[i].id = '';

				if(cta[num - 1] == undefined){
					var nextArticle = cta[cta.length - 1];
					nextArticle.id = 'current';
				
				}else{
					var nextArticle = cta[num - 1];
					nextArticle.id = 'current';
				}
			}
			num++;
		}
		current = nextArticle;
	}


	// Get name input
	var name = document.querySelector('#name');
	// Get email input
	var email = document.querySelector('#email');
	// Right button
	var right = document.querySelector('#right');
	// Left button
	var left = document.querySelector('#left');

	// Event Listeners
	if(name !== null){
	name.addEventListener('blur', minChars, false);
	}
	if(email !== null){
	email.addEventListener('blur', validEmail, false);
	}
	if(right !== null){
	right.addEventListener('click', changeRight, false);
	}
	if(left !== null){
	left.addEventListener('click', changeLeft, false);
	}

}

