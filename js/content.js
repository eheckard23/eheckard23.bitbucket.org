// Create XMLHttpRequest object
var xhr = new XMLHttpRequest();

// When response has loaded function
xhr.onload = function() {

	// Create a variable to store JSON data
	var responseObject = JSON.parse(xhr.responseText);

	// Get keys from responseObject
	var { location, blogPost, event, hikers, about, copyright } = responseObject;

	var locations = '';
	// Loop through array
	for (var i = 0; i < location.length; i++){

		locations += '<article class="cta">';
		locations += `<h1>${location[i].city}, ${location[i].state}</h1>`;
		locations += `<h4>${location[i].title}</h4>`;
		locations += `<p>${location[i].text}</p>`;
		locations += '</article>';
	}
		locations += '<ul>';
		locations += '<li><a href="#" id="left">previous</a></li>';

		for (var i = 0; i < location.length; i++){
			locations += `<li><a href="#" id="selected">${i}</a></li>`;
		}
		locations += '<li><a href="#" id="right">next</a></li>';
		locations += '</ul>';

	// Build string with new content
	var newPost = '';
	newPost += '<h2>Latest Adventures</h2>';
	// Loop through array
	for (var i = 0; i < blogPost.length; i++){

		newPost += '<article>';
		newPost += `<img src="${blogPost[i].imageURL}" alt="${blogPost[i].subtitle}" />`;
		newPost += `<h3> ${blogPost[i].title} </h3>`;
		newPost += `<h5> ${blogPost[i].subtitle} </h5>`;
		newPost += `<p> ${blogPost[i].text} </p>`;
		newPost += `<p><a href="${blogPost[i].moreLink}">Read More</a></p>`;
		newPost += '</article>';
	}
		newPost += '<p><a href="">Load More Adventures</a></p>';

	// Build string with new content
	var newEvents = '';
	// Loop through array
	for (var i = 0; i < event.length; i++){

		newEvents += '<li>';
		newEvents += `<h2>${new Date(event[i].date).getDate() + 1}</h2>`;
		newEvents += `<h5>${event[i].title}</h5>`;
		newEvents += `<p>${event[i].location}</p>`;
		newEvents += '</li>';
	}

	// Build string with new content
	var newHikers = '';
	// Loop through array
	for (var i = 0; i < hikers.length; i++){

		newHikers += '<li>';
		newHikers += `<img src="${hikers[i].imageURL}" alt="${hikers[i].firstname}" />`;
		newHikers += `<h5>${hikers[i].firstname}, ${hikers[i].lastname}</h5>`;
		newHikers += `<p>${hikers[i].city}, ${hikers[i].state}</p>`;
		newHikers += '</li>';
	}

	// Build string with new content
	var aboutContent = '';

		aboutContent += '<h4>About</h4>';
		aboutContent += `<h5>${about.title}</h5>`;
		aboutContent += `<p>${about.text}</p>`;

	// Build string with new content	
	var copyrightInfo = '';

		copyrightInfo += copyright;


	// Update page content
	var cta = document.querySelector('#cta-location');
	cta.innerHTML = locations;

	var blog = document.querySelector('#posts');
	blog.innerHTML = newPost;

	var eventInfo = document.querySelector('#events');
	eventInfo.innerHTML = newEvents;

	var hikersList = document.querySelector('#hikers');
	hikersList.innerHTML = newHikers;

	var aboutInfo = document.querySelector('#about');
	aboutInfo.innerHTML = aboutContent;

	var copy = document.querySelector('#copyright');
	copy.innerHTML = copyrightInfo;

};

// Prepare the xhr request
xhr.open('GET', 'js/data.json', true);
// Send request
xhr.send(null);
